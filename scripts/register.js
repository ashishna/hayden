
/*
Filename: apply.js
Target html: index.html
Purpose: Assignment part 2 - Java
Author: Hayden Smith
Date Written: 19/4/2016
Revisions: Hayden Smith
*/

//"use strict";

function validateForm() {

//	window.alert("This is an alert bud!");

	var errMsg = "";
	var JobCode = "";
	// var result = true;
	// var letters = /^[a-zA-Z]+$/;
	var firstname = document.getElementById("firstname");
	// var lastname = document.getElementById("lastname").value;
	// var age = document.getElementById("age").value;
	// var DOB = document.getElementById("DOB").value;
	// var Day = document.getElementById("Day").value;
	// var Month = document.getElementById("Month").value;
	// var Year = document.getElementById("Year").value;
	// var Gender = document.getElementById("Gender").value;
	// var StreetAddress = document.getElementById("StreetAddress").value;
	// var SuburbTown = document.getElementById("SuburbTown").value;
	// var State = document.getElementById("State").value;
	// var Postcode = document.getElementById("Postcode").value;
	// var personalemail = document.getElementById("personalemail").value;
	// var phone = document.getElementById("personalemail").value;

alert(firstname);


	if (firstname == "") {
		errMsg += "First Name Cannot be empty.\n";
	}

	if (lastname == "") {
		errMsg += "Last Name Cannot be empty.\n";
	}


	if (isNaN(Day)) {
    errMsg = errMsg + "Your day must be a number\n";
    result = false;

	}

	else if (Day < 1){
        errMsg = errMsg + "The day must be above 1!\n";
        result = false;
    }

    else if (Day > 31){
    	errMsg = errMsg + "The day must be less than 31!";
    	result = false;
    }


	if (result) {

	saveJobCode(JobCode);

	}

	return result;


	// alert ("JobCode: " + localStorage.JobCode);

}

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


  function checkForm(form) {

    // regular expression to match required date format
    regs = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;

    if(form.startdate.value != '') {
      if(regs = form.startdate.value.match(re)) {
        // day value between 1 and 31
        if(regs[1] < 1 || regs[1] > 31) {
          alert("Invalid value for day: " + regs[1]);
          form.startdate.focus();
          return false;
        }
        // month value between 1 and 12
        if(regs[2] < 1 || regs[2] > 12) {
          alert("Invalid value for month: " + regs[2]);
          form.startdate.focus();
          return false;
        }
        // year value between 1902 and 2016
        if(regs[3] < 1902 || regs[3] > (new Date()).getFullYear()) {
          alert("Invalid value for year: " + regs[3] + " - must be between 1902 and " + (new Date()).getFullYear());
          form.startdate.focus();
          return false;
        }
      } else {
        alert("Invalid date format: " + form.startdate.value);
        form.startdate.focus();
        return false;
      }
    }
	}



//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


function interceptLinkClickEvent(e) {

	var tag = e.target;
	if(tag.tagName == 'A') {
		var jobId = tag.getAttribute("data");
		localStorage.jobCode = jobId;
		//Redirect to job application page
		document.location = "apply.html";
	}

}


function init(e) {
	 document.addEventListener('click', interceptLinkClickEvent);
	//window.alert("This is an alert!!!!");
	//var regform = document.getElementById("regform"); // get ref to the HTML element
	//regform.onsubmit = validateForm;   //register the event listener
}


window.onload = init();
