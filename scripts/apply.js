/*
Filename: apply.js
Target html: index.html
Purpose: Assignment part 2 - Java
Author: Hayden Smith
Date Written: 19/4/2016
Revisions: Hayden Smith
*/



 "use strict";

function validateForm(e) {

  var state       = document.getElementById("states").value;
	var postCode    = document.getElementById("postCode");
  var otherSkills = document.getElementById("otherSkills");

  var isValid   = true;

  if(postCode != null) {
    postCode = new String(postCode.value);
  } else {
    return;
  }

	switch(state) {
		case 'VIC':
				if(!postCode.startsWith("3") && !postCode.startsWith("8")) {
				 	isValid = false;
				}
			break;
		case 'NSW':
      if(!postCode.startsWith("1") && !postCode.startsWith("2")) {
        isValid = false;
      }
		break;
    case 'TAS':
      if(!postCode.startsWith("7")) {
        isValid = false;
      }
		break;
    case 'QLD':
    if(!postCode.startsWith("4") && !postCode.startsWith("9")) {
      isValid = false;
    }
    break;
    case 'NT':
    if(!postCode.startsWith("0")) {
      isValid = false;
    }
    break;
    case 'WA':
    if(!postCode.startsWith("6")) {
      isValid = false;
    }
    break;
    case 'SA':
    if(!postCode.startsWith("5")) {
      isValid = false;
    }
    break;
    case 'ACT':
    if(!postCode.startsWith("0")) {
      isValid = false;
    }
    break;
    default:
    break;
	}

  if(!isValid) {
    document.getElementById("errorDialog").innerText = "Invalid Post code entered for " + state;
    document.getElementById("errorDialog").showModal();
    return false;
  }

  //Check Other Skills box.
  if(otherSkills != null && otherSkills.checked) {
    var otherSkillsDesc = document.getElementById("otherSkillsDesc").value
    if(otherSkillsDesc == null || otherSkillsDesc.trim() == '' ) {
      document.getElementById("errorDialog").innerText = "Please enter other skills";
      document.getElementById("errorDialog").showModal();
      return false;
    }
  }

  return true;
}

//Submit the form and store data in session storage
function submitFormData() {
  
  if(!validateDateOfBirth() && !validateForm()) {
      return false;
  }


  var streetAddress = getValue(document.getElementById("streetAddress"));
  var suburbTown    = getValue(document.getElementById("suburbTown"));
  var postCode      = getValue(document.getElementById("postCode"));
  var personalemail = getValue(document.getElementById("personalemail"));
  var phone         = getValue(document.getElementById("phone"));

  sessionStorage.streetAddress = streetAddress;
  sessionStorage.suburbTown = suburbTown;
  sessionStorage.postCode = postCode;
  sessionStorage.personalemail = personalemail;
  sessionStorage.phone = phone;

  console.debug ("sessionStorage : " + sessionStorage);
}

//Restore the values from Session Storage if exists.
function restoreSession() {
    document.getElementById("streetAddress").value  = (sessionStorage.streetAddress != null ? sessionStorage.streetAddress : "");
    document.getElementById("suburbTown").value     = (sessionStorage.suburbTown != null ? sessionStorage.suburbTown : "");
    document.getElementById("postCode").value       = (sessionStorage.postCode != null ? sessionStorage.postCode : "");
    document.getElementById("personalemail").value  = (sessionStorage.personalemail != null ? sessionStorage.personalemail : "");
    document.getElementById("phone").value          = (sessionStorage.phone != null ? sessionStorage.phone : "");
}


function init() {

	var jobCode = localStorage.jobCode;
	var jobRefNum = document.getElementById("jobRefNum");
	if(jobRefNum) {
		jobRefNum.readOnly = "true";
		jobRefNum.value = localStorage.jobCode;
	}
  //Restore the previous session.
  restoreSession();

	//document.getElementById("states").onchange = validateForm;

  //handler for error dialog box to handle click as close event.
  document.getElementById("errorDialog").onclick = function() {
    document.getElementById("errorDialog").close();
  }
  //Register Event Handler for Form Submission
  document.getElementById("apply").onclick = submitFormData;
 }

//Utility function to do a null check and return the value.
function getValue(elementObject) {
  if(elementObject != null) {
    return elementObject.value;
  }
  return null;
}

function showErrorMsg(msg) {
  document.getElementById("errorDialog").innerText = msg;
  document.getElementById("errorDialog").showModal();
  return false;
}

function validateDateOfBirth() {
  var dob = document.getElementById("dateOfBirth");
  var regEx = /^(\d{2})\/(\d{2})\/(\d{4})/;

  if(dob) {
    var dobValue = dob.value;
    var result = new String(dobValue).match(regEx);

    if(result == null || result.length == 0) {
      return showErrorMsg("Invalid date of birth format. Must be DD/MM/YYYY");
    }
  }

}
window.onload = init();
